from django.shortcuts import render
from django.http import HttpResponse
import random
# Create your views here.

R_P_S = ["rock", "paper", "scissors"]

def computer_guess() -> str:
    return random.choice(R_P_S)

def play_rock_paper_scissors(p1: str, p2: str) -> str:
    if p1.lower() == p2.lower():
        return "draw"
    outcomes = {("rock", "paper"): "paper",
                ("scissors", "paper"): "scissors",
                ("scissors", "rock"): "rock"}
    inputs = (max(p1, p2), min(p1, p2))
    
    return outcomes[inputs]

def say_hello(request):
    return HttpResponse('''
    <!doctype html>
    <html>
        <body>
            <h3>Greetings</h3>
            <p>Hello, World!</p>
        </body>
    </html>''')

def play_rps(request, user_choice):
    #user_choice = request.GET['user_choice']
    #user_choice = request.GET.get('user_choice', 'rock')
    computer_choice = computer_guess()
    outcome = play_rock_paper_scissors(user_choice, computer_choice)
    context = {"user": user_choice,
               "computer": computer_choice,
               "outcome": outcome}
    return render(request, "hello/rps.html", context)

def show_rps_form(request):
    return render(request, "hello/rps_form.html")

def process_rps_form(request):
    user_choice = request.POST['user_choice']
    computer_choice = computer_guess()
    outcome = play_rock_paper_scissors(user_choice, computer_choice)
    context = {"user": user_choice,
               "computer": computer_choice,
               "outcome": outcome}
    return render(request, "hello/rps.html", context)


questions = [
    "What is the capital of India?",
    "Where is Taj Mahal?",
    "What is the largest planet in our solar system?",
    "What is Golconda Fort?",
]

answers = [
    "delhi",
    "delhi",
    "jupiter",
    "hyderabad",
]

def quiz_view(request):
    question = random.choice(questions)
    context = {'question': question}
    return render(request, 'hello/quiz.html', context)





# quiz/views.py
def check_answer_view(request):
    if request.method == 'POST':
        user_answer = request.POST.get('answer')
        if user_answer in answers:
            result = "Correct answer!"
        else:
            result = "Incorrect answer!"
        context = {'result': result}
        return render(request, 'hello/quizresult.html', context)    
    


names=["arun","ajay","kohli","aravind","ashokan","avesh","virat","rohit","kiran"]


def gen(request):
    return render(request,"hello/word_game.html")
last=[]
def replace(k:int,n1:str,n2:str):
    new_=[i for i in names if i[0]==n1 and len(i) >= k]
    for i in new_:
        re=i.replace(n2,"")
        last.append(re)
    new_.clear()
    return last
def new(request):
    req_len=int(request.POST["c1"])
    start=request.POST["c2"]
    exchange=request.POST["co"]
    request.session['size'] = req_len
    request.session['include']= start
    request.session['exclude'] = exchange
    m=replace(req_len,start,exchange)
    rans=random.choice(m)
    last.clear()
    context={ "m":m,
        "l1":last,
             "l2":rans}
    return render(request,"hello/game.html",context)