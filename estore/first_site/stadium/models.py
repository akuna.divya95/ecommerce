from django.db import models

class Score(models.Model):
    name = models.CharField(max_length=255)
    score = models.IntegerField()