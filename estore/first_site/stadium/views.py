from django.shortcuts import render
from .models import Score


def scorecard(request):
    team1_name = "India"
    team1_runs = 0
    team1_wickets = 0
    team1_overs = 0

    team2_name = "Australia"
    team2_runs = 0
    team2_wickets = 0
    team2_overs = 0

    context = {
        'team1_name': team1_name,
        'team1_runs': team1_runs,
        'team1_wickets': team1_wickets,
        'team1_overs': team1_overs,
        'team2_name': team2_name,
        'team2_runs': team2_runs,
        'team2_wickets': team2_wickets,
        'team2_overs': team2_overs,
    }
    
    return render(request, 'stadium/scorecard.html', context)