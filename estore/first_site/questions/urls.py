from django.urls import path
from . import views


urlpatterns = [
    path('quizform', views.random_questions , name='random_questions'),
    path('quiz_process_form', views.process_quiz, name='process_quiz')   
]