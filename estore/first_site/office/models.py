from django.db import models
# Create your models here.

class Employee(models.Model):
    emp_name = models.CharField(max_length = 100)
    emp_no = models.CharField(max_length = 10)
    salary = models.CharField(max_length = 30)
    

    def str(self):
        return f'{self.emp_name} - {self.emp_no} - {self.salary}'

class Employee_attendance(models.Model):
    date = models.DateField()
    present = models.CharField(max_length = 50)
    employee =models.ForeignKey(Employee, on_delete=models.CASCADE)


def str(self):
    return f'(roll){self.employee.emp_no} - {self.date} - {self.present}'