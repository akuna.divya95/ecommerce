from django.urls import path
from . import views

urlpatterns = [
     path('all',views.Employee_list,name="Employee_list"),
     path('add', views.add_employee, name='add_employee'),
     path('detail/<int:id>', views.employee_detail, name='employee_detail'),
     path('delete/<int:id>', views.delete_employee, name='delete_employee'),
     path('edit/<int:id>', views.update_employee, name='update_employee'),
     path('detail/<int:id>/add_attencence',views.add_attendence,name="add_attendence"),
     path('details/<int:id>/edit_attendence/<int:att_id>',views.edit_attendence,name="edit_attendence"),

]