from django.urls import path


urlpatterns = [
    path("quizform/" views.random_questions , name = "show_question")
    path("quiz_process_form" views.process_quiz, name = "process_answer")   
]