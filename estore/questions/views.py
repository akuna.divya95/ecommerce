from django.shortcuts import render


import random


questions = {
    "What is the capital of India?" : 'delhi',
    "Where is Taj Mahal?" : 'delhi',
    "What is the largest planet in our solar system?" : 'jupiter',
    "What is Golconda Fort?" : 'hyderabad',
}

question_ = []


def random_questions(request):
    questions_list = list(questions.keys())
    random_question = random.choice(questions_list)
    question_.append(random_question)
    context = {
        'question': random_question
    }
    return render(request, 'questions/first_show.html', context)


def process_quiz (request):
    user_input = request.POST("user_answer")
    question_displayed = question_[0]
    question_.pop(0)
    if questions[question_displayed] == user_input:
        context1 = {
            "question" : question_displayed,
            "user_given" : user_input , 
            "result"  : "winner"
        }
        return render(request, "questions/winner.html" , context1)
    
    else:
        actual_answer = questions[question_displayed]
        context2 = {
            "question" : question_displayed,
            "user_given" : user_input,
            "act" : actual_answer ,
            "result" : "loser"  
        }
        return render(request, "questions/loser.html" , context2)
        


# Create your views here.
